# How to create an arch container

Run `whoami` in a terminal and verify that it returns your username. Then run these commands on the host:

```bash
mkdir ~/scratch/
./build-image.sh
./create-container.sh <container name>
./enter-container.sh # after this step, we are inside the container
```
