# syntax=docker/dockerfile:1
FROM archlinux
ARG username
ARG dialoutgid
WORKDIR /arch-container
COPY . .
RUN pacman -Syu --noconfirm
RUN pacman -S --noconfirm ansible
RUN ansible-galaxy collection install kewlfft.aur
RUN ansible-playbook --extra-vars "username=${username} dialoutgid=${dialoutgid}" ./playbook.yml
WORKDIR /home/${username}
CMD tail -f /dev/null
